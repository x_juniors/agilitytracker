# Agility Tracker #
### Система управления проектами ###

### [База данных, скрипты, диаграммы на Dropbox](https://www.dropbox.com/sh/dxshfv9ucvhqpgj/AACfABJmUX6i217e8S4DnzBua?dl=0) ###
### Диаграмма БД ###
![Agility Tracker Physical Schema.png](https://bitbucket.org/repo/jXyBjM/images/4212914524-Agility%20Tracker%20Physical%20Schema.png)

Создать файл с именем 
```
#!path
ConnectionStrings.config
```
с данными, изменив строку соединения на свою (data source) :

```
#!xml
<connectionStrings>
  <add name="AgilityTrackerEntities" connectionString="metadata=res://*/AgilityTrackerModel.csdl|res://*/AgilityTrackerModel.ssdl|res://*/AgilityTrackerModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=.\SQLEXPRESS;initial catalog=AgilityTracker;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
</connectionStrings>
```